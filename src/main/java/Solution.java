import java.util.*;

public class Solution {
    enum Sol {
        OK, LESS, LOT
    }

    static String[] solve(String in, int maxLenSMS) {
        String[] words = in.split(" +");

        if(words.length == 1 && words[0].length() <= maxLenSMS) {
            return words;
        }

        int l = 0, r = in.length() + in.length() / maxLenSMS;
        while(l + 1 < r) {
            int m = (l + r) / 2;

            Sol ans = constructAns(words, m, maxLenSMS, null);
            if(ans == Sol.OK) {
                r = m;
            } else if(ans == Sol.LESS) {
                r = m;
            } else {
                l = m;
            }
        }
        String[] res = new String[r];

        if(r == 1) {
            StringBuilder value = new StringBuilder();
            for(var word : words) {
                value.append(word).append(' ');
            }
            value.delete(value.length() - 1, value.length());
            res[0] = value.toString();
            return res;
        }

        constructAns(words, r, maxLenSMS, res);
        return res;
    }

    static Sol constructAns(String[] words, int cntOfWords, int maxLenSMS, String[] res) {
        List<StringBuilder> builder = new ArrayList<>();
        builder.add(new StringBuilder(words[0]));

        int it = 1;
        while(it < words.length) {
            String suff = " " + (builder.size()) + "/" + cntOfWords;
            if(builder.get(builder.size() - 1).length() + words[it].length() + 1 + suff.length() <= maxLenSMS) {
                builder.get(builder.size() - 1).append(" ").append(words[it]);
            } else if(it == 1 && builder.get(builder.size() - 1).length() + suff.length() > maxLenSMS) {
                return Sol.LOT;
            } else {
                builder.get(builder.size() - 1).append(suff);
                builder.add(new StringBuilder(words[it]));
            }
            if(builder.size() > cntOfWords) {
                return Sol.LOT;
            }
            it++;
        }

        String suffLast = " " + (builder.size()) + "/" + cntOfWords;
        builder.get(builder.size() - 1).append(suffLast);

        if(builder.size() > cntOfWords) {
            return Sol.LOT;
        } else if(builder.size() < cntOfWords) {
            return Sol.LESS;
        }
        if(res != null) {
            for(int i = 0; i < res.length; i++) {
                res[i] = builder.get(i).toString();
            }
        }
        return Sol.OK;
    }
}