import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestSolution {
    @Test
    public void test1() {
        Solution.solve("The first was none other than Mikhail Berlioz", 13);
        String[] expected = {"The first 1/6",
                "was none 2/6",
                "other 3/6",
                "than 4/6",
                "Mikhail 5/6",
                "Berlioz 6/6"};

        Assertions.assertArrayEquals(expected, Solution.solve("The first was none other than Mikhail Berlioz", 13));
    }

    @Test
    public void test2() {

        String[] expected = {"At the 1/9",
                "sunset hour 2/9",
                "of one warm 3/9",
                "spring day 4/9",
                "two men were 5/9",
                "to be seen 6/9",
                "at 7/9",
                "Patriarch's 8/9",
                "Ponds. 9/9"};
        Assertions.assertArrayEquals(expected, Solution.solve("At    the sunset hour   of   one warm spring day   two" +
                " men were to be seen at Patriarch's Ponds.", 16));

    }

    @Test
    public void test3() {
        String[] expected = {"There was 1/8",
                "an 2/8",
                "oddness 3/8",
                "about 4/8",
                "that 5/8",
                "terrible 6/8",
                "day in 7/8",
                "May 8/8"};
        Assertions.assertArrayEquals(expected, Solution.solve("There was an oddness about that terrible day in May", 13));
    }

    @Test
    public void test4small() {
        String[] expected = { "the great day" };

        Assertions.assertArrayEquals(expected, Solution.solve("the great day", 20));
    }


    private final int cntStress = 100_000;
    private final int seedStress = 38371;
    Random rand;

    @BeforeEach
    void startStress() {
        rand = new Random(seedStress);
    }

    @Test
    public void testLarge() {
        StringBuilder strBuilder = new StringBuilder();
        int maxWordSize = rand.nextInt(64);

        for (int cnt = 0; cnt < cntStress; cnt++) {
            for (int i = 0; i < maxWordSize; i++) {
                strBuilder.append((char) rand.nextInt(128));
            }
            strBuilder.append(' ');
        }

        String value = strBuilder.toString();

        int maxLen = rand.nextInt(128);
        String[] res = Solution.solve(value, maxLen);

        boolean flag = true;
        for(var s : res) {
            if(s.length() > maxLen) {
                flag = false;
                break;
            }
        }
        Assertions.assertTrue(flag);
    }
}
